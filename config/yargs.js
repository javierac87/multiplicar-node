const opt = {
    base: {
        demand: true,
        alias: 'b'
    },
    limite: {
        alias: 'l',
        default: 10
    }
}

const argv = require('yargs')
                .command('listar', 'Imprime en consola la tabla de multiplicación', opt)
                .command('crear', 'Crea un archivo con la tabla de multiplicación', opt)
                .help()
                .argv;

module.exports = {
    argv
}